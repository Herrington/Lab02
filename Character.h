#pragma once
#include <string>
#include <iostream>
#include "BaseCharacter.h"

class Character
	:public BaseCharacter
{
private:
	int experience;
	std::string personality;
public:
	Character() = default;
	Character(float, std::string, std::string, int, int, std::string);
	~Character();
	virtual void Describe();
	int getExp();
	std::string getPers();
	virtual void Edit();
	virtual void LoadData(const std::string &Data);
	virtual std::string GetData();
};

