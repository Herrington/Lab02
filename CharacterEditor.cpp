#include "stdafx.h"
#include "CharacterEditor.h"
#include <string>
#include <iostream>
#include "BaseCharacter.h"

CharacterEditor::CharacterEditor()
{
}


CharacterEditor::~CharacterEditor()
{
}

BaseCharacter CharacterEditor::createCharacter()
{
	float hp;
	std::string name, race, per;
	int kp, exp;
	std::cout << "Creating new hero\n";
	std::cout << "Name: "; std::cin >> name;
	std::cout << "Race: "; std::cin >> race;
	std::cout << "KP: "; std::cin >> kp;
	std::cout << "EXP: "; std::cin >> exp;
	std::cout << "Health points: "; std::cin >> hp;
	std::cout << "Personality: "; std::cin >> per;

	BaseCharacter *hero = new Character(hp, name, race, kp, exp, per);
	Characters.push_back(hero);

	return *hero;
}

BaseCharacter CharacterEditor::createEnemy()
{
	float hp;
	std::string name, race;
	int kp, exp_g; 
	bool elit;
	std::cout << "Creating new enemy\n";
	std::cout << "Name: "; std::cin >> name;
	std::cout << "Race: "; std::cin >> race;
	std::cout << "KP: "; std::cin >> kp;
	std::cout << "EXP given by enemy: "; std::cin >> exp_g;
	std::cout << "Health points: "; std::cin >> hp;
	std::cout << "Is it an elite enemy? Input 1 or 0: "; std::cin >> elit;

	BaseCharacter *enemy = new Enemy(hp, name, race, kp, exp_g, elit);
	Characters.push_back(enemy);

	return *enemy;
}

void CharacterEditor::DisplayCharacters()
{
	for (unsigned int i = 0; i<Characters.size(); ++i)
	{
		int type = Characters[i]->CharacterType();
		std::cout << "\n\nLp. " << i+1;
		Characters[i]->Describe();
	}
}
void CharacterEditor::saveToFile(std::string filePath)
{
	std::fstream File;
	File.open(filePath, std::ios::out | std::ios::app);
	if (File.good())
	{
		for (size_t i = 0; i<Characters.size(); ++i)
		{
			File << Characters[i]->GetData();
			if (i<Characters.size() - 1) File << '\n';
		}
		File.close();
	}
}

std::vector<BaseCharacter*> CharacterEditor::loadFromFile(std::string filePath)
{
	std::fstream File;
	std::string Line;
	File.open("CharactersData.txt", std::ios::in);

	if (File.good())
	{
		while (std::getline(File, Line))
		{
			int Type;
			std::stringstream ss;
			ss << Line; ss >> Type;
			if (Type == 1) Characters.push_back(new Character);
			else if (Type == 2) Characters.push_back(new Enemy);
			if (Type == 1 || Type == 2)
			{
				Characters.back()->LoadData(Line);
			}
		}
		File.close();
	}
	return Characters;
}

void CharacterEditor::CharacterEdit()
{
	int number;
	std::cout << "\nWhich character do you wish to edit?";
	std::cout << "\nInput character number to edit: ";
	std::cin >> number;
	Characters[number-1]->Edit();
}
