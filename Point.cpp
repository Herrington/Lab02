#include "stdafx.h"
#include "Point.h"



Point::Point()
{
}

Point::Point(int x=0, int y=0)
{
	this->x = x;
	this->y = y;
	if (x > 100) this->x -= 100;
	if (y > 100) this->y -= 100;
	//std::cout << "Konstruktor";
}

Point::Point(const Point & point)
{
	x = point.x;
	y = point.y;
	//std::cout << "Konstr kopiujacy";
}

Point::~Point()
{
}

void Point::test(const Point point)
{
}

void Point::test1(const Point & point)
{
}

void Point::test2(const Point * point)
{
}

bool Point::operator>(const Point & punkt1)
{
	if (this->x > punkt1.x && this->y > punkt1.y) return true;
	else return false;
}

bool Point::operator<(const Point & punkt1)
{
	if (this->x < punkt1.x && this->y < punkt1.y) return true;
	else return false;
}

bool Point::operator==(const Point & punkt1)
{
	if (this->x == punkt1.x && this->y == punkt1.y) return true;
	else return false;
}

bool Point::operator>=(const Point & punkt1)
{
	if (this->x >= punkt1.x && this->y >= punkt1.y) return true;
	else return false;
}

bool Point::operator<=(const Point & punkt1)
{
	if (this->x <= punkt1.x && this->y <= punkt1.y) return true;
	else return false;
}
