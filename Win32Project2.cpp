// Win32Project2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "BaseCharacter.h"
#include <iostream>
#include <string>
#include "Character.h"
#include "Enemy.h"
#include "CharacterEditor.h"
#ifdef _WIN32
#include <Windows.h>
#endif
#include <cstdlib>
#include "Point.h"
#include <vector>
#include "Tile.h"
#include "Grass.h"
#include "Mountain.h"
#include "Tree.h"
#include <math.h>
#include <time.h>
#include <memory>

Point operator+(const Point& punkt1, const Point& punkt2)
{
	Point suma = Point(0,0);
	suma.x = punkt1.x + punkt2.x;
	suma.y = punkt1.y + punkt2.y;
	if (suma.x > 100) suma.x -= 100;
	if (suma.y > 100) suma.y -= 100;
	return Point(suma.x, suma.y);
}
Point operator*(const Point& punkt1, const int skalar)
{
	Point iloczyn = Point(0, 0);
	iloczyn.x = punkt1.x*skalar;
	iloczyn.y = punkt1.y*skalar;
	if (iloczyn.x > 100) iloczyn.x -= 100;
	if (iloczyn.y > 100) iloczyn.y -= 100;
	return Point(iloczyn.x, iloczyn.y);
}
Point operator-(const Point& punkt1, const Point& punkt2)
{
	Point roznica = Point(0, 0);
	roznica.x = punkt1.x - punkt2.x;
	roznica.y = punkt1.y - punkt2.y;
	if (roznica.x > 100) roznica.x -= 100;
	if (roznica.y > 100) roznica.y -= 100;
	return Point(roznica.x, roznica.y);
}
std::ostream& operator<<(std::ostream& out, Point& punkt) 
{
	out << "(" << punkt.x << ", " << punkt.y << ")";
	return out;
}
std::ostream& operator<<(std::ostream& out, Tile& tile)
{
	out << tile.Terrain;
	return out;
}
void TEST() {
	Point punkt1 = Point(100, 64);
	Point punkt2 = Point(3, 40);
	std::cout << punkt1 << "\n";
	std::cout << punkt2 << "\n";

	Point punkt3 = punkt1 + punkt2;
	std::cout << punkt3 << "\n";

	Point punkt4 = punkt1 - punkt2;
	std::cout << punkt4 << "\n";

	Point punkt5 = punkt1*10;
	std::cout << punkt5;

	if (punkt1 > punkt2) std::cout << "\npunkt1 jest wiekszy niz punkt2";
	if (punkt1 < punkt2) std::cout << "\npunkt1 jest mniejszy niz punkt2";
	if (punkt1 >= punkt2) std::cout << "\npunkt1 jest wiekszy lub rowny punkt2";
	if (punkt1 <= punkt2) std::cout << "\npunkt1 jest mniejszy lub rowny punkt2";
	if (punkt1 == punkt2) std::cout << "\npunkt1 jest rowny punkt2";
}

int main()
{

	//TEST();
	/*
	Point punkt1 = Point(100, 64);
	Point *punkt2 = new Point(100, 64);

	std::cout << "\n\n";

	punkt1.test(punkt1);
	punkt1.test1(punkt1);
	
	std::cin.get();
	*/

	/*
	CharacterEditor Editor;
	std::fstream File;
	while (true) {
		std::cout << "What do you want to do?\n";
		char Option;
		std::cout << "[1] Create new character\n";
		std::cout << "[2] Create new enemy\n";
		std::cout << "[3] Display characters\n";
		std::cout << "[4] Edit character\n";
		std::cout << "[5] Save characters to file\n";
		std::cout << "[6] Load characters from file\n";
		std::cout << "[7] Exit the program\n\n";
		std::cin >> Option;

		if (Option == '1')
		{
			Editor.createCharacter();
		}
		else if (Option == '2')
		{
			Editor.createEnemy();
		}
		else if (Option == '3')
		{
			Editor.DisplayCharacters();
			std::cin.get();
		}
		else if (Option == '4')
		{
			Editor.CharacterEdit();
		}
		else if (Option == '5')
		{
			Editor.saveToFile("CharactersData.txt");
		}
		else if (Option == '6')
		{
			Editor.loadFromFile("CharactersData.txt");
		}
		else if (Option == '7')
		{
			exit(0);
		}
		getchar();
		system("cls");
	}
	*/
	srand(time(NULL));
	int high, wide;
	std::cout << "Podaj szerokosc mapy: ";
	std::cin >> wide;
	std::cout << "Podaj wysokosc mapy: ";
	std::cin >> high;
	getchar();

	typedef std::vector<std::shared_ptr<Tile> > AxisX;
	std::vector<AxisX> Map;

	for (int i = 0; i<high; i++)
	{
		AxisX vec;
		for (int j = 0; j < wide; j++)
		{
			int choice = rand() % 300 + 1;
			if (choice >= 1 && choice <= 50)
			{
				std::shared_ptr<Tile> tile(new Mountain(j, i, 'M'));
				vec.push_back(tile);
			}
			if (choice >= 51 && choice <= 135)
			{
				std::shared_ptr<Tile> tile(new Tree(j, i, 't'));
				vec.push_back(tile);
			}
			if (choice >= 136 && choice <= 300)
			{
				std::shared_ptr<Tile> tile(new Grass(j, i, '.'));
				vec.push_back(tile);
			}
		}
		Map.push_back(vec);
	}

	for (int i = 0; i<high; i++)
	{
		std::cout << "\n";
		for (int j = 0; j < wide; j++) 
		{
			Map[j][i]->Describe();
			std::cout << " ";
		}
	}
	getchar();

    return 0;
}

