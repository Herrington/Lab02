#pragma once
#include "BaseCharacter.h"
#include "Character.h"
#include "Enemy.h"
#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>

class CharacterEditor
{
private:
	std::vector<BaseCharacter*> Characters;
public:
	CharacterEditor();
	~CharacterEditor();
	BaseCharacter createCharacter();
	BaseCharacter createEnemy();
	void DisplayCharacters();
	void saveToFile(std::string filePath);
	std::vector<BaseCharacter*> loadFromFile(std::string filePath);
	void CharacterEdit();
};

