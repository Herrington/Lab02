#include "stdafx.h"
#include "Enemy.h"
#ifdef _WIN32
#include <conio.h>
#endif


Enemy::Enemy(float hp, std::string name, std::string race, int kp, int exp_g, bool elit)
	:BaseCharacter(hp, name, race, kp)
{
	this->exp_given = exp_g;
	this->elite = elit;
	this->Type = 2;
}

Enemy::~Enemy()
{
}

void Enemy::Describe()
{
	std::cout << "\nName: " << Name;
	std::cout << "\nRace: " << Race;
	std::cout << "\nHealth points: " << HeathPoints;
	std::cout << "\nKP: " << Kp;
	std::cout << "\nExp given: " << exp_given;
	if (elite) std::cout << "\nElite class enemy ";
	else std::cout << "\nNormal enemy";
}

int Enemy::getExpG()
{
	return exp_given;
}

bool Enemy::getElite()
{
	return elite;
}

void Enemy::Edit()
{
	float hp;
	std::string name, race;
	int kp, exp_g, option;
	bool elit;

		std::cout << "\nWhat do you want to edit?";
		std::cout << "\n1. Name";
		std::cout << "\n2. Race";
		std::cout << "\n3. Health points";
		std::cout << "\n4. KP";
		std::cout << "\n5. Exp given";
		std::cout << "\n6. Elite status";
		std::cout << "\nInput number of attribute you wish to edit: ";
		option = std::cin.get();

		switch (option)
		{
			case '1':
			{
				std::cin >> name;
				this->Name = name;
				break;
			}
			case '2':
			{
				std::cin >> race;
				this->Race = race;
				break;
			}
			case '3':
			{
				std::cin >> hp;
				this->HeathPoints = hp;
				break;
			}
			case '4':
			{
				std::cin >> kp;
				this->Kp = kp;
				break;
			}
			case '5':
			{
				std::cin >> exp_g;
				this->exp_given = exp_g;
				break;
			}
			case '6':
			{
				std::cin >> elit;
				this->elite = elit;
				break;
			}
			default:
				std::cout << "Please input right number to choose attributes you wish to edit.\n";
					break;
		}
}

void Enemy::LoadData(const std::string & Data)
{
	std::stringstream ss;
	ss << Data;
	ss >> Type; ss >> Name; ss >> Race; ss >> HeathPoints; ss >> Kp; ss >> exp_given; ss >> elite;
}

std::string Enemy::GetData()
{
	std::stringstream ss;
	ss << Type << ' ' << Name << ' ' << Race << ' ' << HeathPoints << ' ' << Kp << ' ' << exp_given << ' ' << elite;
	return ss.str();
}
