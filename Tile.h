#pragma once
#include "Point.h"
class Tile :
	public Point
{
protected:
	char Terrain;
public:
	Tile();
	Tile(int, int, char);
	~Tile();
	virtual void Describe();
	friend std::ostream& operator<<(std::ostream& out, Tile& tile);
};

