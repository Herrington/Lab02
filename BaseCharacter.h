#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <sstream>

class BaseCharacter
{
protected:
	float HeathPoints;
	std::string Name;
	std::string Race;
	int Kp;
	int Type;
public:
	BaseCharacter() = default;
	BaseCharacter(float, std::string, std::string, int);
	virtual ~BaseCharacter();
	int CharacterType();
	virtual void Describe();
	int getHp();
	int getKp();
	std::string getName();
	std::string getRace();
	virtual void Edit();
	virtual void LoadData(const std::string &Data);
	virtual std::string GetData();
};

