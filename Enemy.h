#pragma once
#include "BaseCharacter.h"
#include <iostream>
#include <string>
#include <sstream>

class Enemy
	:public BaseCharacter
{
private:
	int exp_given;
	bool elite;
public:
	Enemy() = default;
	Enemy(float, std::string, std::string, int, int, bool);
	~Enemy();
	virtual void Describe();
	int getExpG();
	bool getElite();
	virtual void Edit();
	virtual void LoadData(const std::string &Data);
	virtual std::string GetData();
};

