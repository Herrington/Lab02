#pragma once
#include <iostream>
#include <ostream>

class Point
{
	int x;
	int y;
public:
	Point();
	Point(int, int);
	Point(const Point &point);
	~Point();

	void test(const Point point);
	void test1(const Point& point);
	void test2(const Point* point);

	bool operator>(const Point& punkt1);
	bool operator<(const Point& punkt1);
	bool operator==(const Point& punkt1);
	bool operator>=(const Point& punkt1);
	bool operator<=(const Point& punkt1);
	
	friend Point operator+(const Point& punkt1, const Point& punkt2);
	friend Point operator-(const Point& punkt1, const Point& punkt2);
	friend Point operator*(const Point& punkt1, const int skalar);
	friend std::ostream& operator<<(std::ostream& out, Point& punkt);
};

