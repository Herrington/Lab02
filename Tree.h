#pragma once
#include "Tile.h"
class Tree :
	public Tile
{
public:
	Tree(int, int, char);
	~Tree();
	virtual void Describe();
};

