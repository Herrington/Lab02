#include "stdafx.h"
#include "Character.h"
#ifdef _WIN32
#include <conio.h>
#endif

Character::Character(float hp, std::string name, std::string race, int kp, int exp, std::string per)
	:BaseCharacter(hp, name, race, kp)
{
	this->experience = exp;
	this->personality = per;
	this->Type = 1;
}


Character::~Character()
{
}

void Character::Describe()
{
	std::cout << "\nName: " << Name;
	std::cout << "\nRace: " << Race;
	std::cout << "\nHealth points: " << HeathPoints;
	std::cout << "\nKP: " << Kp;
	std::cout << "\nExperience points: " << experience;
	std::cout << "\nPersonality: " << personality;
}

int Character::getExp()
{
	return experience;
}

std::string Character::getPers()
{
	return personality;
}

void Character::Edit()
{
	float hp;
	std::string name, race, per;
	int kp, exp, option;
		std::cout << "\nWhat do you want to edit?";
		std::cout << "\n1. Name";
		std::cout << "\n2. Race";
		std::cout << "\n3. Health points";
		std::cout << "\n4. KP";
		std::cout << "\n5. Experience";
		std::cout << "\n6. Personality";
		std::cout << "\nInput number of attribute you wish to edit: ";
		option = std::cin.get();

		switch (option)
		{
		case '1':
		{
			std::cin >> name;
			this->Name = name;
			break;
		}
		case '2':
		{
			std::cin >> race;
			this->Race = race;
			break;
		}
		case '3':
		{
			std::cin >> hp;
			this->HeathPoints = hp;
			break;
		}
		case '4':
		{
			std::cin >> kp;
			this->Kp = kp;
			break;
		}
		case '5':
		{
			std::cin >> exp;
			this->experience = exp;
			break;
		}
		case '6':
		{
			std::getline(std::cin, per);
			this->personality = per;
			break;
		}
		default:
			std::cout << "Please input right number to choose attributes you wish to edit.\n";
			break;
		}
}
void Character::LoadData(const std::string & Data)
{
	std::stringstream ss;
	ss << Data;
	ss >> Type; ss >> Name; ss >> Race; ss >> HeathPoints; ss >> Kp; ss >> experience; ss >> personality;
}

std::string Character::GetData()
{
	std::stringstream ss;
	ss << Type << ' ' << Name << ' ' << Race << ' ' << HeathPoints << ' ' << Kp << ' ' << experience << ' ' << personality;
	return ss.str();
}

