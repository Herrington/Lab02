#pragma once
#include "Tile.h"
class Mountain :
	public Tile
{
public:
	Mountain(int, int, char);
	~Mountain();
	virtual void Describe();
};
