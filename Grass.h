#pragma once
#include "Tile.h"
class Grass :
	public Tile
{
public:
	Grass(int, int, char);
	~Grass();
	virtual void Describe();
};

